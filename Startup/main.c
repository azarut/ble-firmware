/******************************************************************************

 @file       main.c

 @brief main entry of the BLE stack sample application.

 Group: CMCU, SCS
 Target Device: CC2640R2

 ******************************************************************************
 
 Copyright (c) 2013-2017, Texas Instruments Incorporated
 All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:

 *  Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

 *  Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.

 *  Neither the name of Texas Instruments Incorporated nor the names of
    its contributors may be used to endorse or promote products derived
    from this software without specific prior written permission.

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 ******************************************************************************
 Release Name: simplelink_cc2640r2_sdk_01_50_00_58
 Release Date: 2017-10-17 18:09:51
 *****************************************************************************/

/*******************************************************************************
 * INCLUDES
 */

#include <xdc/runtime/Error.h>

#include <ti/drivers/Power.h>
#include <ti/drivers/power/PowerCC26XX.h>
#include <ti/drivers/SPI.h>
#include <ti/drivers/spi/SPICC26XXDMA.h>
#include <ti/drivers/dma/UDMACC26XX.h>
#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/knl/Clock.h>
#include <ti/display/Display.h>

#include <icall.h>
#include "hal_assert.h"
#include "bcomdef.h"
#include "peripheral.h"
#include "simple_peripheral.h"

/* Header files required to enable instruction fetch cache */
#include <inc/hw_memmap.h>
#include <driverlib/vims.h>

#include "../RES/AquariusCoin.bmp.c"
#include "../RES/Augur.bmp.c"
#include "../RES/Bitcoin.bmp.c"
#include "../RES/BitConnect.bmp.c"
#include "../RES/BitShares.bmp.c"
/*#include "../RES/ByteCoin.bmp.c"
#include "../RES/Dash.bmp.c"
#include "../RES/Decred.bmp.c"
#include "../RES/EOS.bmp.c"
#include "../RES/Ethereum.bmp.c"
#include "../RES/Golem.bmp.c"
#include "../RES/HODL.bmp.c"
#include "../RES/Iconomi.bmp.c"
#include "../RES/IOTA.bmp.c"
#include "../RES/LanaCoin.bmp.c"
#include "../RES/Litecoin.bmp.c"
#include "../RES/Monero.bmp.c"
#include "../RES/NEM.bmp.c"
#include "../RES/Netko-coin.bmp.c"
#include "../RES/NevaCoin.bmp.c"
#include "../RES/Ripple.bmp.c"
#include "../RES/Siacoin.bmp.c"
#include "../RES/Steem.bmp.c"
#include "../RES/Stratis.bmp.c"
#include "../RES/Suncontract.bmp.c"
#include "../RES/TajCoin.bmp.c"
#include "../RES/Universa.bmp.c"
#include "../RES/Waves.bmp.c"
#include "../RES/Xaurum.bmp.c"
#include "../RES/Zcash.bmp.c"*/





#ifdef USE_RCOSC
#include "rcosc_calibration.h"
#endif //USE_RCOSC

#ifndef USE_DEFAULT_USER_CFG

#include "ble_user_config.h"

// BLE user defined configuration
#ifdef ICALL_JT
icall_userCfg_t user0Cfg = BLE_USER_CFG;
#else  /* ! ICALL_JT */
bleUserCfg_t user0Cfg = BLE_USER_CFG;
#endif /* ICALL_JT */

#endif // USE_DEFAULT_USER_CFG


/*******************************************************************************
 * MACROS
 */
#define SSD1351_CMD_SETCOLUMN       0x15
#define SSD1351_CMD_SETROW          0x75
#define SSD1351_CMD_WRITERAM        0x5C
#define SSD1351_CMD_READRAM         0x5D
#define SSD1351_CMD_SETREMAP        0xA0
#define SSD1351_CMD_STARTLINE       0xA1
#define SSD1351_CMD_DISPLAYOFFSET   0xA2
#define SSD1351_CMD_DISPLAYALLOFF   0xA4
#define SSD1351_CMD_DISPLAYALLON    0xA5
#define SSD1351_CMD_NORMALDISPLAY   0xA6
#define SSD1351_CMD_INVERTDISPLAY   0xA7
#define SSD1351_CMD_FUNCTIONSELECT  0xAB
#define SSD1351_CMD_DISPLAYOFF      0xAE
#define SSD1351_CMD_DISPLAYON       0xAF
#define SSD1351_CMD_PRECHARGE       0xB1
#define SSD1351_CMD_DISPLAYENHANCE  0xB2
#define SSD1351_CMD_CLOCKDIV        0xB3
#define SSD1351_CMD_SETVSL          0xB4
#define SSD1351_CMD_SETGPIO         0xB5
#define SSD1351_CMD_PRECHARGE2      0xB6
#define SSD1351_CMD_SETGRAY         0xB8
#define SSD1351_CMD_USELUT          0xB9
#define SSD1351_CMD_PRECHARGELEVEL  0xBB
#define SSD1351_CMD_VCOMH           0xBE
#define SSD1351_CMD_CONTRASTABC     0xC1
#define SSD1351_CMD_CONTRASTMASTER  0xC7
#define SSD1351_CMD_MUXRATIO        0xCA
#define SSD1351_CMD_COMMANDLOCK     0xFD
#define SSD1351_CMD_HORIZSCROLL     0x96
#define SSD1351_CMD_STOPSCROLL      0x9E
#define SSD1351_CMD_STARTSCROLL     0x9F


#define SSD1351WIDTH 128
#define SSD1351HEIGHT 96  // SET THIS TO 96 FOR 1.27"!

#define WIDTH 128
#define HEIGHT 96  // SET THIS TO 96 FOR 1.27"!


/*******************************************************************************
 * CONSTANTS
 */
void OutImage(uint32_t ImgMAss);
void DisplayON(void);
void DisplayOFF(void);
const uint32_t* pics_index[] =
        {
                AquariusCoin_bmp,
                Augur_bmp,
                Bitcoin_bmp,
                BitConnect_bmp,
                BitShares_bmp,
               /* ByteCoin_bmp,
                Dash_bmp,
                Decred_bmp,
                EOS_bmp,
                Ethereum_bmp,
                Golem_bmp,
                HODL_bmp,
                Iconomi_bmp,
                IOTA_bmp,
                LanaCoin_bmp,
                Litecoin_bmp,
                Monero_bmp,
                NEM_bmp,
                Netko_coin_bmp,
                NevaCoin_bmp,
                Ripple_bmp,
                Siacoin_bmp,
                Steem_bmp,
                Stratis_bmp,
                Suncontract_bmp,
                TajCoin_bmp,
                Universa_bmp,
                Waves_bmp,
                Xaurum_bmp,
                Zcash_bmp*/
        };
/*******************************************************************************
 * TYPEDEFS
 */

/*******************************************************************************
 * LOCAL VARIABLES
 */

/*******************************************************************************
 * GLOBAL VARIABLES
 */


/*******************************************************************************
 * EXTERNS
 */

extern void AssertHandler(uint8 assertCause, uint8 assertSubcause);

extern Display_Handle dispHandle;

/*******************************************************************************
 * @fn          Main
 *
 * @brief       Application Main
 *
 * input parameters
 *
 * @param       None.
 *
 * output parameters
 *
 * @param       None.
 *
 * @return      None.
 *
 */
static PIN_Handle hSpiPin= NULL;
SPI_Handle spihandle;
SPI_Params params;
SPI_Transaction transaction;
PIN_Id csnPin1 = PIN_ID(CC2640R2_LAUNCHXL_SPI_FLASH_CS);
uint8_t txBufff[] = "Hello World";


void writeData_noCS( unsigned char data )
{

transaction.count = sizeof(data);
transaction.txBuf = &data;
transaction.rxBuf = NULL;
SPI_transfer(spihandle, &transaction);
//PIN_setOutputValue(hSpiPin,CC2640R2_LAUNCHXL_SPI_FLASH_CS, 1);
}

void writeData( unsigned char data )
{
PIN_setOutputValue(hSpiPin,SSD1351_DC_PIN, 1);
PIN_setOutputValue(hSpiPin,CC2640R2_LAUNCHXL_SPI_FLASH_CS, 0);
transaction.count = sizeof(data);
transaction.txBuf = &data;
transaction.rxBuf = NULL;
SPI_transfer(spihandle, &transaction);
PIN_setOutputValue(hSpiPin,CC2640R2_LAUNCHXL_SPI_FLASH_CS, 1);
}

void writeCommand( unsigned char cmd )
{
PIN_setOutputValue(hSpiPin,SSD1351_DC_PIN, 0);
PIN_setOutputValue(hSpiPin,CC2640R2_LAUNCHXL_SPI_FLASH_CS, 0);
// Configure the transaction
transaction.count = sizeof(cmd);
transaction.txBuf = &cmd;
transaction.rxBuf = NULL;

// Open the SPI and perform transfer to the first slave
//handle = SPI_open(Board_SPI0, &params);
SPI_transfer(spihandle, &transaction);
//SPI_control(spihandle, SPICC26XXDMA_SET_CSN_PIN, &csnPin1);
//SPI_transfer(spihandle, &transaction);
PIN_setOutputValue(hSpiPin,CC2640R2_LAUNCHXL_SPI_FLASH_CS, 1);
//PIN_setOutputValue(hSpiPin,SSD1351_DC_PIN, 0);
//suc=bspSpiWrite( &cmd,sizeof(cmd) );
}

void goTo(int x, int y) {
    if ((x >= SSD1351WIDTH) || (y >= SSD1351HEIGHT)) return;

    // set x and y coordinate
    writeCommand(SSD1351_CMD_SETCOLUMN);
    writeData(x);
    writeData(SSD1351WIDTH-1);

    writeCommand(SSD1351_CMD_SETROW);
    writeData(y);
    writeData(SSD1351HEIGHT-1);

    writeCommand(SSD1351_CMD_WRITERAM);
}

void DrawBMP(uint16_t x, uint16_t y, uint8_t index)
    {

    writeCommand(SSD1351_CMD_SETCOLUMN);
    writeData(x);
    writeData(x+47);
    writeCommand(SSD1351_CMD_SETROW);
    writeData(y);
    writeData(y+47);
    // fill!
    writeCommand(SSD1351_CMD_WRITERAM);

    //for (uint16_t i=48*48; i > 0; i--)
    //{
    //           writeData((uint8_t)((pics_index[index])[i-1] >> 16));
    //           writeData((uint8_t)((pics_index[index])[i-1] >> 8));
    //           writeData((uint8_t)((pics_index[index])[i-1]));
    //}

    const uint32_t* img_buf = (pics_index[index]);
    for (uint16_t k = 0; k<48; k++)
    {
        for (uint16_t i = 0; i<48; i++)
        {
            writeData(((uint8_t)(img_buf[i+(47-k)*48] >> 16)) >> 2);
            writeData(((uint8_t)(img_buf[i+(47-k)*48] >> 8)) >> 2);
            writeData(((uint8_t)(img_buf[i+(47-k)*48])) >> 2);
            //uint8_t r, g, b;
            //b = (img_buf[i+(47-k)*48])|0x1f;
           // g = (((img_buf[i+(47-k)*48])&0x18)<<3)|0x3f;
            //r = (((img_buf[i+(47-k)*48])&0x07)<<5)|0x1f;
            ////writeData(r);
           //writeData(g);
            //writeData(b);

        }
    }
}


void rawFillRect(uint16_t x, uint16_t y, uint16_t w, uint16_t h, uint32_t fillcolor) {
    // Bounds check
    if ((x >= SSD1351WIDTH) || (y >= SSD1351HEIGHT))
        return;

    // Y bounds check
    if (y+h > SSD1351HEIGHT)
    {
     //   h = SSD1351HEIGHT - y - 1;
    }

    // X bounds check
    if (x+w > SSD1351WIDTH)
    {
      //  w = SSD1351WIDTH - x - 1;
    }
    // set location
    writeCommand(SSD1351_CMD_SETCOLUMN);
    writeData(x);
    writeData(x+w-1);
    writeCommand(SSD1351_CMD_SETROW);
    writeData(y);
    writeData(y+h-1);
    // fill!
    writeCommand(SSD1351_CMD_WRITERAM);

    for (uint16_t i=0; i < w*h; i++) {
        writeData((uint8_t)(fillcolor >> 16));
        writeData((uint8_t)(fillcolor >>8));
        writeData((uint8_t)fillcolor);
    }
}


void drawPixel(int16_t x, int16_t y, uint32_t color)
{
    // Bounds check.
    if ((x >= SSD1351WIDTH) || (y >= SSD1351HEIGHT)) return;
    if ((x < 0) || (y < 0)) return;

    goTo(x, y);

    // setup for data
    writeData((uint8_t)(color >> 16));
            writeData((uint8_t)(color >>8));
            writeData((uint8_t)color);
}

void oled_init(void)
{
// Init SPI and specify non-default parameters
uint8_t ctrst[] = {0x02, 0x03, 0x04, 0x05,
                   0x06, 0x07, 0x08, 0x09,
                   0x0A, 0x0B, 0x0C, 0x0D,
                   0x0E, 0x0F, 0x10, 0x11,
                   0x12, 0x13, 0x15, 0x17,
                   0x19, 0x1B, 0x1D, 0x1F,
                   0x21, 0x23, 0x25, 0x27,
                   0x2A, 0x2D, 0x30, 0x33,
                   0x36, 0x39, 0x3C, 0x3F,
                   0x42, 0x45, 0x48, 0x4C,
                   0x50, 0x54, 0x58, 0x5C,
                   0x60, 0x64, 0x68, 0x6C,
                   0x70, 0x74, 0x78, 0x7D,
                   0x82, 0x87, 0x8C, 0x91,
                   0x96, 0x9B, 0xA0, 0xA5,
                   0xAA, 0xAF, 0xB4};

uint8_t cntr = 0;


PIN_setOutputValue(hSpiPin,SSD1351_RES_PIN, 0);

SPI_Params_init(&params);
params.bitRate = 10000000;
params.frameFormat = SPI_POL0_PHA0;
params.mode = SPI_MASTER;
SPI_init();
// Configure the transaction
//transaction.count = sizeof(txBuf);
//transaction.txBuf = txBuf;
//transaction.rxBuf = NULL;

// Open the SPI and perform transfer to the first slave
PIN_setOutputValue(hSpiPin,SSD1351_RES_PIN, 1);
spihandle = SPI_open(Board_SPI0, &params);

/*    while(1)
    {
        writeCommand(0x55);// <---- this while problem
    }*/

 writeCommand(SSD1351_CMD_COMMANDLOCK);  // set command lock
 writeData(0x12);
 writeCommand(SSD1351_CMD_COMMANDLOCK);  // set command lock
 writeData(0xB1);

 writeCommand(SSD1351_CMD_DISPLAYOFF);      // 0xAE

 writeCommand(SSD1351_CMD_CLOCKDIV);      // 0xB3
 writeData(0xF1);              // 7:4 = Oscillator Frequency, 3:0 = CLK Div Ratio (A[3:0]+1 = 1..16)

 writeCommand(SSD1351_CMD_MUXRATIO);
 writeData(0x5F);

 writeCommand(SSD1351_CMD_DISPLAYOFFSET);
 writeData(0);

 writeCommand(SSD1351_CMD_STARTLINE );
 writeData(0);

 writeCommand(SSD1351_CMD_SETREMAP);
 writeData(0xB4);

 writeCommand(SSD1351_CMD_SETGPIO );
 writeData(0x00);

 writeCommand(SSD1351_CMD_FUNCTIONSELECT);
 writeData(0x01);

 writeCommand(SSD1351_CMD_SETVSL );
 writeData(0xA0);
 writeData(0xB5);
 writeData(0x55);

 writeCommand(SSD1351_CMD_CONTRASTABC);
    writeData(0xC8);
    writeData(0x80);
    writeData(0x8A);
 writeCommand(SSD1351_CMD_CONTRASTMASTER);
       writeData(0x6f);
writeCommand(SSD1351_CMD_SETGRAY);
        for(cntr=0; cntr < sizeof(ctrst); cntr++)
        {
            writeData(ctrst[cntr]);
        }
writeCommand(SSD1351_CMD_PRECHARGE);        // 0xB1
writeData(0x32);
writeCommand(SSD1351_CMD_DISPLAYENHANCE);        // 0xB1
writeData(0xA4);
writeData(0x00);
writeData(0x00);
writeCommand(SSD1351_CMD_PRECHARGELEVEL);        // 0xB1
writeData(0x17);
writeCommand(SSD1351_CMD_PRECHARGE2);        // 0xB1
writeData(0x01);
writeCommand(SSD1351_CMD_VCOMH);        // 0xB1
writeData(0x05);
writeCommand(SSD1351_CMD_NORMALDISPLAY);        // 0xB1
PIN_setOutputValue(hSpiPin,SSD1351_PWR_PIN, 1);
//writeCommand(SSD1351_CMD_DISPLAYON);
  /*  writeCommand(SSD1351_CMD_COMMANDLOCK);  // set command lock
    writeData(0x12);
    writeCommand(SSD1351_CMD_COMMANDLOCK);  // set command lock
    writeData(0xB1);

    writeCommand(SSD1351_CMD_DISPLAYOFF);       // 0xAE

    writeCommand(SSD1351_CMD_CLOCKDIV);         // 0xB3
    writeCommand(0xF1);                         // 7:4 = Oscillator Frequency, 3:0 = CLK Div Ratio (A[3:0]+1 = 1..16)

    writeCommand(SSD1351_CMD_MUXRATIO);
    writeData(0x5F);

    writeCommand(SSD1351_CMD_SETREMAP);
    writeData(0xB4);

    writeCommand(SSD1351_CMD_SETCOLUMN);
    writeData(0x00);
    writeData(0x7F);
    writeCommand(SSD1351_CMD_SETROW);
    writeData(0x00);
    writeData(0x7F);

    writeCommand(SSD1351_CMD_STARTLINE);        // 0xA1
      writeData(0x00);
    writeCommand(SSD1351_CMD_DISPLAYOFFSET);    // 0xA2
    writeData(0x0);

    writeCommand(SSD1351_CMD_SETGPIO);
    writeData(0x00);

    writeCommand(SSD1351_CMD_FUNCTIONSELECT);
    writeData(0x01); // internal (diode drop)
    //writeData(0x01); // external bias

//    writeCommand(SSSD1351_CMD_SETPHASELENGTH);
//    writeData(0x32);

    writeCommand(SSD1351_CMD_PRECHARGE);        // 0xB1
    writeCommand(0x17);

    writeCommand(SSD1351_CMD_VCOMH);            // 0xBE
    writeCommand(0x05);

    writeCommand(SSD1351_CMD_INVERTDISPLAY);    // 0xA6

    writeCommand(SSD1351_CMD_CONTRASTABC);
    writeData(0xC8);
    writeData(0x80);
    writeData(0xC8);

    writeCommand(SSD1351_CMD_CONTRASTMASTER);
    writeData(0x0F);

    writeCommand(SSD1351_CMD_SETVSL );
    writeData(0xA0);
    writeData(0xB5);
    writeData(0x55);

    writeCommand(SSD1351_CMD_PRECHARGE2);
    writeData(0x01);

    writeCommand(SSD1351_CMD_DISPLAYON);      */  //--turn on oled panel
}



const PIN_Config BoardGp[] = {

    CC2640R2_LAUNCHXL_PIN_BTN1 | PIN_INPUT_EN | PIN_PULLUP | PIN_IRQ_BOTHEDGES | PIN_HYSTERESIS,          /* Button is active low */
    CC2640R2_LAUNCHXL_PIN_BTN2 | PIN_INPUT_EN | PIN_PULLUP | PIN_IRQ_BOTHEDGES | PIN_HYSTERESIS,          /* Button is active low */

    CC2640R2_LAUNCHXL_SPI_FLASH_CS | PIN_GPIO_OUTPUT_EN | PIN_GPIO_HIGH | PIN_PUSHPULL | PIN_INPUT_DIS | PIN_DRVSTR_MED,
    CC2640R2_LAUNCHXL_SPI0_CLK | PIN_GPIO_OUTPUT_EN | PIN_GPIO_LOW | PIN_PUSHPULL | PIN_INPUT_DIS | PIN_DRVSTR_MED,
    CC2640R2_LAUNCHXL_SPI0_MOSI | PIN_GPIO_OUTPUT_EN | PIN_GPIO_LOW | PIN_PUSHPULL | PIN_INPUT_DIS | PIN_DRVSTR_MED,

    SSD1351_BS0_PIN | PIN_GPIO_OUTPUT_EN | PIN_GPIO_LOW | PIN_PUSHPULL | PIN_DRVSTR_MAX,  /* External flash chip select */
    SSD1351_BS1_PIN | PIN_GPIO_OUTPUT_EN | PIN_GPIO_LOW | PIN_PUSHPULL | PIN_DRVSTR_MAX,  /* External flash chip select */
    SSD1351_PWR_PIN | PIN_GPIO_OUTPUT_EN | PIN_GPIO_LOW | PIN_PUSHPULL | PIN_DRVSTR_MAX,  /* External flash chip select */
    SSD1351_RES_PIN | PIN_GPIO_OUTPUT_EN | PIN_GPIO_LOW | PIN_PUSHPULL | PIN_DRVSTR_MAX,  /* External flash chip select */
    SSD1351_DC_PIN  | PIN_GPIO_OUTPUT_EN | PIN_GPIO_LOW | PIN_PUSHPULL | PIN_DRVSTR_MAX,  /* External flash chip select */
    SSD1351_RW_PIN  | PIN_GPIO_OUTPUT_EN | PIN_GPIO_LOW | PIN_PUSHPULL | PIN_DRVSTR_MAX,  /* External flash chip select */
    SSD1351_ERD_PIN | PIN_GPIO_OUTPUT_EN | PIN_GPIO_LOW | PIN_PUSHPULL | PIN_DRVSTR_MAX,  /* External flash chip select */
    SSD1351_D3_PIN | PIN_GPIO_OUTPUT_EN | PIN_GPIO_LOW | PIN_PUSHPULL | PIN_DRVSTR_MAX,  /* External flash chip select */
    SSD1351_D4_PIN | PIN_GPIO_OUTPUT_EN | PIN_GPIO_LOW | PIN_PUSHPULL | PIN_DRVSTR_MAX,  /* External flash chip select */
    SSD1351_D5_PIN | PIN_GPIO_OUTPUT_EN | PIN_GPIO_LOW | PIN_PUSHPULL | PIN_DRVSTR_MAX,  /* External flash chip select */
    SSD1351_D6_PIN | PIN_GPIO_OUTPUT_EN | PIN_GPIO_LOW | PIN_PUSHPULL | PIN_DRVSTR_MAX,  /* External flash chip select */
    SSD1351_D7_PIN | PIN_GPIO_OUTPUT_EN | PIN_GPIO_LOW | PIN_PUSHPULL | PIN_DRVSTR_MAX,  /* External flash chip select */

    PIN_TERMINATE
};


void OutImage(uint32_t ImgMAss)
{
      DrawBMP(0,47,(uint8_t)(ImgMAss>>24));
      DrawBMP(0,0,(uint8_t)(ImgMAss>>16));
      DrawBMP(48,48,(uint8_t)(ImgMAss>>8));
      DrawBMP(48,0,(uint8_t)(ImgMAss));
}

void DisplayON(void)
{

    //PIN_setOutputValue(hSpiPin,SSD1351_PWR_PIN, 1);
    writeCommand(SSD1351_CMD_DISPLAYON);

}

void DisplayOFF(void)
{
    writeCommand(SSD1351_CMD_DISPLAYOFF);
   // PIN_setOutputValue(hSpiPin,SSD1351_PWR_PIN, 0);

}
int main()
{


    /* Register Application callback to trap asserts raised in the Stack */
  RegisterAssertCback(AssertHandler);
   // #ifdef USE_RCOSC

 //   #endif // USE_RCOSC
  PIN_init(BoardGp);
  // Enable iCache prefetching
  VIMSConfigure(VIMS_BASE, TRUE, TRUE);
  // Enable cache
  VIMSModeSet(VIMS_BASE, VIMS_MODE_ENABLED);
  //SPI_open();

#if defined( POWER_SAVING ) || defined( USE_FPGA )
  /* Set constraints for Standby, powerdown and idle mode */
  // PowerCC26XX_SB_DISALLOW may be redundant
  Power_setConstraint(PowerCC26XX_SB_DISALLOW);
  Power_setConstraint(PowerCC26XX_IDLE_PD_DISALLOW);
#endif // POWER_SAVING | USE_FPGA

#ifdef ICALL_JT
  /* Update User Configuration of the stack */
  user0Cfg.appServiceInfo->timerTickPeriod = Clock_tickPeriod;
  user0Cfg.appServiceInfo->timerMaxMillisecond  = ICall_getMaxMSecs();
#endif  /* ICALL_JT */
  /* Initialize ICall module */



  ICall_init();

  /* Start tasks of external images - Priority 5 */
  ICall_createRemoteTasks();

  /* Kick off profile - Priority 3 */
  RCOSC_enableCalibration();

  oled_init();
  rawFillRect(0,0,128,96,0xFFFFFF);
  //rawFillRect(0,48,128,47,0xffff03);
  //rawFillRect(0,0,128,96,0xffffff);
  /*rawFillRect(0,0,128,96,0xffffff);*/
  OutImage(0x01030200);
  //drawPixel(64,64,0xffffff);
  GAPRole_createTask();

  ProjectZero_createTask();
  //BCHeck_createTask();
  /* enable interrupts and start SYS/BIOS */
  BIOS_start();

  return 0;
}


/*******************************************************************************
 * @fn          AssertHandler
 *
 * @brief       This is the Application's callback handler for asserts raised
 *              in the stack.  When EXT_HAL_ASSERT is defined in the Stack
 *              project this function will be called when an assert is raised,
 *              and can be used to observe or trap a violation from expected
 *              behavior.
 *
 *              As an example, for Heap allocation failures the Stack will raise
 *              HAL_ASSERT_CAUSE_OUT_OF_MEMORY as the assertCause and
 *              HAL_ASSERT_SUBCAUSE_NONE as the assertSubcause.  An application
 *              developer could trap any malloc failure on the stack by calling
 *              HAL_ASSERT_SPINLOCK under the matching case.
 *
 *              An application developer is encouraged to extend this function
 *              for use by their own application.  To do this, add hal_assert.c
 *              to your project workspace, the path to hal_assert.h (this can
 *              be found on the stack side). Asserts are raised by including
 *              hal_assert.h and using macro HAL_ASSERT(cause) to raise an
 *              assert with argument assertCause.  the assertSubcause may be
 *              optionally set by macro HAL_ASSERT_SET_SUBCAUSE(subCause) prior
 *              to asserting the cause it describes. More information is
 *              available in hal_assert.h.
 *
 * input parameters
 *
 * @param       assertCause    - Assert cause as defined in hal_assert.h.
 * @param       assertSubcause - Optional assert subcause (see hal_assert.h).
 *
 * output parameters
 *
 * @param       None.
 *
 * @return      None.
 */
void AssertHandler(uint8 assertCause, uint8 assertSubcause)
{
#if !defined(Display_DISABLE_ALL)
  // Open the display if the app has not already done so
  if ( !dispHandle )
  {
    dispHandle = Display_open(Display_Type_LCD, NULL);
  }

  Display_print0(dispHandle, 0, 0, ">>>STACK ASSERT");
#endif // ! Display_DISABLE_ALL

  // check the assert cause
  switch (assertCause)
  {
    case HAL_ASSERT_CAUSE_OUT_OF_MEMORY:
#if !defined(Display_DISABLE_ALL)
      Display_print0(dispHandle, 0, 0, "***ERROR***");
      Display_print0(dispHandle, 2, 0, ">> OUT OF MEMORY!");
#endif // ! Display_DISABLE_ALL
      break;

    case HAL_ASSERT_CAUSE_INTERNAL_ERROR:
      // check the subcause
      if (assertSubcause == HAL_ASSERT_SUBCAUSE_FW_INERNAL_ERROR)
      {
#if !defined(Display_DISABLE_ALL)
        Display_print0(dispHandle, 0, 0, "***ERROR***");
        Display_print0(dispHandle, 2, 0, ">> INTERNAL FW ERROR!");
#endif // ! Display_DISABLE_ALL
      }
      else
      {
#if !defined(Display_DISABLE_ALL)
        Display_print0(dispHandle, 0, 0, "***ERROR***");
        Display_print0(dispHandle, 2, 0, ">> INTERNAL ERROR!");
#endif // ! Display_DISABLE_ALL
      }
      break;

    case HAL_ASSERT_CAUSE_ICALL_ABORT:
#if !defined(Display_DISABLE_ALL)
      Display_print0(dispHandle, 0, 0, "***ERROR***");
      Display_print0(dispHandle, 2, 0, ">> ICALL ABORT!");
#endif // ! Display_DISABLE_ALL
      HAL_ASSERT_SPINLOCK;
      break;

    case HAL_ASSERT_CAUSE_ICALL_TIMEOUT:
#if !defined(Display_DISABLE_ALL)
      Display_print0(dispHandle, 0, 0, "***ERROR***");
      Display_print0(dispHandle, 2, 0, ">> ICALL TIMEOUT!");
#endif // ! Display_DISABLE_ALL
      HAL_ASSERT_SPINLOCK;
      break;

    case HAL_ASSERT_CAUSE_WRONG_API_CALL:
#if !defined(Display_DISABLE_ALL)
      Display_print0(dispHandle, 0, 0, "***ERROR***");
      Display_print0(dispHandle, 2, 0, ">> WRONG API CALL!");
#endif // ! Display_DISABLE_ALL
      HAL_ASSERT_SPINLOCK;
      break;

  default:
#if !defined(Display_DISABLE_ALL)
      Display_print0(dispHandle, 0, 0, "***ERROR***");
      Display_print0(dispHandle, 2, 0, ">> DEFAULT SPINLOCK!");
#endif // ! Display_DISABLE_ALL
      HAL_ASSERT_SPINLOCK;
  }

  return;
}


/*******************************************************************************
 * @fn          smallErrorHook
 *
 * @brief       Error handler to be hooked into TI-RTOS.
 *
 * input parameters
 *
 * @param       eb - Pointer to Error Block.
 *
 * output parameters
 *
 * @param       None.
 *
 * @return      None.
 */
void smallErrorHook(Error_Block *eb)
{
  for (;;);
}
/*******************************************************************************
 */
